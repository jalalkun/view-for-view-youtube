package xyz.jalalkun.viewforviewyoutube.profile

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import xyz.jalalkun.viewforviewyoutube.ui.theme.ViewForViewComposeTheme

@Composable
fun ProfileMainScreen (){
    ViewForViewComposeTheme(darkTheme = false){
        Scaffold{
            Text(text = "halo Profile Main Screen",
                modifier = Modifier
                    .padding(0.dp)
                    .fillMaxWidth()
                    .padding(16.dp)
            )

        }
    }
}