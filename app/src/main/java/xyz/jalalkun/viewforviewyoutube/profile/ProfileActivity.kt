package xyz.jalalkun.viewforviewyoutube.profile

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import xyz.jalalkun.viewforviewyoutube.utils.Cons.ROUTE
import xyz.jalalkun.viewforviewyoutube.utils.Cons.TENTANG
import xyz.jalalkun.viewforviewyoutube.ui.theme.ViewForViewComposeTheme

class ProfileActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val route = intent.getStringExtra(ROUTE)
        setContent {
            when(route){
                TENTANG -> {
                    TentangView()
                }

                else -> {
                    this.finish()
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ViewForViewComposeTheme {
    }
}