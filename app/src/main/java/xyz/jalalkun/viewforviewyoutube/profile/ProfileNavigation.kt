package xyz.jalalkun.viewforviewyoutube.profile

import viewforviewyoutube.R
import xyz.jalalkun.viewforviewyoutube.utils.Cons.TENTANG

sealed class ProfileNavigation(var route: String, var icon: Int, var title: String){
    object Tentang: ProfileNavigation(TENTANG, R.drawable.ic_baseline_home_24, "Tentang")
}
