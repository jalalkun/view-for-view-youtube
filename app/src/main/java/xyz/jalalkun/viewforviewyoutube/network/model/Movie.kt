package xyz.jalalkun.viewforviewyoutube.network.model

data class Movie(
    var title: String?,
    var image: String?
)
