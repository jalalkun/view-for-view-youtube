package xyz.jalalkun.viewforviewyoutube.network

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import viewforviewyoutube.BuildConfig
import xyz.jalalkun.viewforviewyoutube.network.model.Movie

interface MovieInterface {
    @GET("volley_array.json")
    suspend fun getMovies() : Response<List<Movie>>
}

val movieApiService: MovieInterface by lazy {
    Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BuildConfig.BASE_URL)
        .build()
        .create(MovieInterface::class.java)
}