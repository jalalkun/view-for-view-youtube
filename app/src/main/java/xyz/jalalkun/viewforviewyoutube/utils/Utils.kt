package xyz.jalalkun.viewforviewyoutube.utils

import android.content.res.Configuration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import xyz.jalalkun.viewforviewyoutube.utils.Cons.TEMA

object Utils {
    fun getRandomId(): String{
        val datas = arrayListOf<String>()
        datas.add("MOAReRFwrAs")
        datas.add("rEXQWabRhSQ")
        datas.add("dCyWaLPHeW0")
        datas.add("0kv59j7ZKc4")
        datas.add("sTiwY-_i-qk")
        return datas.random()
    }

    fun <T> T?.notNull(f: (it: T) -> Unit) {
        if (this != null) f(this)
    }

    @Composable
    fun isDark(): Boolean{
        return when(Sessions.getInt(TEMA)){
            Configuration.UI_MODE_NIGHT_NO -> false
            Configuration.UI_MODE_NIGHT_YES -> true
            else -> isSystemInDarkTheme()
        }
    }

    fun randomString(length: Int) : String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }


}