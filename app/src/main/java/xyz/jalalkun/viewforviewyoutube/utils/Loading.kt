package xyz.jalalkun.viewforviewyoutube.utils

import androidx.compose.foundation.layout.*
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import viewforviewyoutube.R

/**
 * @param [withText] true jika ingin menampilan text
 */
@Composable
fun Loading(withText: Boolean? = false) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(6.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        LinearProgressIndicator(
            modifier = Modifier
                .fillMaxWidth()
                .height(6.dp)
        )
        if (withText!!)Text(text = stringResource(id = R.string.loading))
    }
}
