package xyz.jalalkun.viewforviewyoutube.utils

object Cons {
    const val ROUTE = "route"
    /**
     * Route name for main
     * @see [xyz.jalalkun.viewforviewyoutube.main.NavigationItem]
     */
    const val HOME = "home"
    const val PLAY = "play"
    const val PROFILE = "profile"
    /**
     * Route name for Auth
     * @see xyz.jalalkun.viewforviewyoutube.auth.NavigationItem
     */
    const val SPLASH = "splash"
    const val LOGIN = "login"
    /**
     * Profile route name
     */
    const val PROFILE_MAIN_SCREEN = "profile_main_screen"
    const val TENTANG = "tentang"

    /**
     * Sessions
     */
    const val TEMA = "tema"
    const val TOKEN = "token"
}