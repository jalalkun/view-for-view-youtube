package xyz.jalalkun.viewforviewyoutube.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import xyz.jalalkun.viewforviewyoutube.model.User
import xyz.jalalkun.viewforviewyoutube.utils.Cons.TEMA

object Sessions {
    private lateinit var sharedPref : SharedPreferences
    private const val name = "view_for_view"
    fun init (context: Context){
        sharedPref = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    fun clearData(){
        with(sharedPref.edit()){
            clear()
            commit()
        }
    }

    fun setUser(user: User){
        with(sharedPref.edit()){
            putString("user", Gson().toJson(user))
            commit()
        }
    }

    fun getUser(): User?{
        val gson = Gson()
        val json: String? = sharedPref.getString("user", "")
        val type = object : TypeToken<User?>() {}.type
        return gson.fromJson(json, type)
    }

    fun setBoolean(boolean: Boolean){
        with(sharedPref.edit()){
            putBoolean(TEMA, boolean)
            commit()
        }
    }

    fun getBoolean(key: String) =  sharedPref.getBoolean(key, false)

    fun setString(key: String, value: String){
        with(sharedPref.edit()){
            putString(key, value)
            commit()
        }
    }

    fun getString(key: String) = sharedPref.getString(key, "")

    fun setInt(key: String){
        with(sharedPref.edit()){
            putInt(key, 0)
            commit()
        }
    }

    fun getInt(key: String) = sharedPref.getInt(key, 0)
}