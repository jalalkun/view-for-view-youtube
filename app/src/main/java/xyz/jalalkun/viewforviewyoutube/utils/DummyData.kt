package xyz.jalalkun.viewforviewyoutube.utils

import android.content.Context
import xyz.jalalkun.viewforviewyoutube.model.User

object DummyData {
    fun fakeUser(){
        Sessions.clearData()
        val user = User(
            id = "26",
            name = "Saucang Enak",
            username = "saucang",
            email = "saucang@gmail.com",
            avatar = "https://www.cultura.id/wp-content/uploads/2019/05/pokemon_detective_pikachu_review_indonesia.jpg",
            total_point = 100,
            access_token = "jkkasndfnkahsiewrelruASEFj982934asdf"
        )
        Sessions.setUser(user)
    }
}