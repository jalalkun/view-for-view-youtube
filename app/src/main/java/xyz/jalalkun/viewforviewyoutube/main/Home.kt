package xyz.jalalkun.viewforviewyoutube.main

import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import viewforviewyoutube.R
import xyz.jalalkun.viewforviewyoutube.play.PlayActivity
import xyz.jalalkun.viewforviewyoutube.ui.theme.ViewForViewComposeTheme
import xyz.jalalkun.viewforviewyoutube.utils.Sessions

@Composable
fun HomeView(navController: NavController) {
    val tagar = "HomeView"
    var hiddenSalam by remember {
        mutableStateOf(false)
    }
    val user = Sessions.getUser()
    val scrollState = rememberScrollState()
    ViewForViewComposeTheme() {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
                .scrollable(
                    state = scrollState,
                    orientation = Orientation.Vertical
                )
        ) {

            Card(
                modifier = Modifier
                    .padding(bottom = 8.dp)
                    .clip(shape = RoundedCornerShape(4.dp))
                    .fillMaxWidth()
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_baseline_person_24),
                        contentDescription = null,
                        modifier = Modifier
                            .height(62.dp)
                            .width(62.dp)
                            .clip(shape = RoundedCornerShape(50))
                    )
                    Column(
                        modifier = Modifier.padding(start = 4.dp)
                    ) {
                        user?.name?.let {
                            Text(
                                text = it,
                                style = TextStyle(fontSize = 18.sp, fontWeight = Bold)
                            )
                        }
                    }

                    Column(
                        horizontalAlignment = Alignment.End,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Text(
                            text = user?.total_point.toString(),
                            fontSize = 32.sp,
                            fontWeight = Bold
                        )
                        Text(
                            text = stringResource(id = R.string.point_name),
                            fontStyle = FontStyle.Italic,
                            fontSize = 12.sp
                        )
                    }
                }
            }

            if (!hiddenSalam) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp, bottom = 8.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    val name = user?.name.toString()
                    Text(text = stringResource(R.string.salam_home, name))
                    Column(
                        horizontalAlignment = Alignment.End,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        IconButton(onClick = { hiddenSalam = !hiddenSalam }) {
                            Icon(Icons.Filled.Close, "close salam")
                        }
                    }
                }
            }

            Image(
                painter = painterResource(id = R.drawable.header),
                contentDescription = null,
                modifier = Modifier
                    .height(180.dp)
                    .fillMaxWidth()
                    .clip(shape = RoundedCornerShape(4.dp)),
                contentScale = ContentScale.Crop
            )

            Spacer(modifier = Modifier.height(16.dp))

            Text(
                text = stringResource(id = R.string.sambutan_home),
                style = MaterialTheme.typography.body2,
                overflow = TextOverflow.Ellipsis,
                textAlign = TextAlign.Center
            )

            Button(
                onClick = {
                    navController.context.startActivity(
                        Intent(
                            navController.context,
                            PlayActivity::class.java
                        )
                    )
//                    navController.navigate(PLAY) {
//                        popUpTo(HOME) { inclusive = true }
//                    }
                },
                modifier = Modifier
                    .align(alignment = Alignment.CenterHorizontally)
                    .padding(16.dp)
            ) {
                Text(
                    text = "Tap To Play"
                )
            }

//            val itemList = arrayListOf<String>()
//            for (i in 0.until((20..50).random())) {
//                itemList.add("$i")
//            }
//            Log.e(tagar, "HomeView: item size ${itemList.size}")
//            LazyColumn(modifier = Modifier
//                .fillMaxHeight()
//                .padding(bottom = 48.dp)) {
//                items(items = itemList, itemContent = { posisi ->
//                    Card(
//                        modifier = Modifier
//                            .padding(bottom = 8.dp)
//                            .clip(shape = RoundedCornerShape(4.dp))
//                            .fillMaxWidth()
//                    ) {
//                        Row(
//                            modifier = Modifier
//                                .fillMaxWidth()
//                                .padding(
//                                    start = 8.dp,
//                                    end = 8.dp,
//                                    top = 4.dp,
//                                    bottom = 4.dp
//                                )
//                        ) {
//                            Text(text = posisi)
//                            Text(text = "12:00 12-08-2021")
//                            Text(
//                                text = "2 Point",
//                                modifier = Modifier.fillMaxWidth(),
//                                textAlign = TextAlign.End
//                            )
//                        }
//                    }
//                })
//            }
        }
    }
}

@Preview
@Composable
fun HomePreview() {
    HomeView(navController = NavHostController(LocalContext.current))
}