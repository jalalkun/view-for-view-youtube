package xyz.jalalkun.viewforviewyoutube.main

import viewforviewyoutube.R
import xyz.jalalkun.viewforviewyoutube.utils.Cons.HOME
import xyz.jalalkun.viewforviewyoutube.utils.Cons.PLAY
import xyz.jalalkun.viewforviewyoutube.utils.Cons.PROFILE

sealed class NavigationItem(var route: String, var icon: Int, var title: String){
    object Home : NavigationItem(HOME, R.drawable.ic_baseline_home_24, "Home")
    object Play: NavigationItem(PLAY, R.drawable.ic_baseline_play_arrow_24, "Play")
    object Profile: NavigationItem(PROFILE, R.drawable.ic_baseline_person_24, "Profile")
}