package xyz.jalalkun.viewforviewyoutube.main

import android.content.Context
import android.content.Intent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import viewforviewyoutube.R
import xyz.jalalkun.viewforviewyoutube.utils.Cons.ROUTE
import xyz.jalalkun.viewforviewyoutube.utils.Cons.TENTANG
import xyz.jalalkun.viewforviewyoutube.profile.ProfileActivity

@Composable
fun ProfileView(navController: NavController, context: Context){
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.black))
            .wrapContentSize(Alignment.Center)
    ) {
        Text(
            text = "Profile View",
            fontWeight = FontWeight.Bold,
            color = Color.White,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            fontSize = 25.sp
        )

        Button(
            onClick = {
                val intent = Intent(context, ProfileActivity::class.java)
                intent.putExtra(ROUTE, TENTANG)
                context.startActivity(intent)
            },
            modifier = Modifier
                .padding(8.dp)
                .background(color = Color.Green)
        ) {
            Text(
                text = "Tentang"
            )
        }
    }
}