package xyz.jalalkun.viewforviewyoutube.model

data class User(
    var id: String?,
    var name: String?,
    var username: String?,
    var email: String?,
    var avatar: String?,
    var total_point: Int? = 0,
    var access_token: String?
)
