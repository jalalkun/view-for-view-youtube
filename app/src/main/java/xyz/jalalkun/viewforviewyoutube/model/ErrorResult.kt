package xyz.jalalkun.viewforviewyoutube.model

data class ErrorResult(val message: String, val errorCode: Int)