package xyz.jalalkun.viewforviewyoutube.model

data class Rest<D>(
    var status: String? = null,
    var message: String? = null,
    var data: D? = null
)
