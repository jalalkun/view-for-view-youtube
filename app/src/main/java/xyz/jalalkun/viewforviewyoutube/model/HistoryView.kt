package xyz.jalalkun.viewforviewyoutube.model

data class HistoryView(
    var id: String?,
    var point: Int? = 0,
    var campaign_id: String?,
    var time_create: String?
)