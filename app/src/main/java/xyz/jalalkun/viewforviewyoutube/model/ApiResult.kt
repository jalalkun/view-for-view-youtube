package xyz.jalalkun.viewforviewyoutube.model

import retrofit2.Response

data class ApiResult<D>(
    var data: D? = null,
    var response: Response<D>? = null,
    var exception: Exception? = null
) {

    val isSuccess: Boolean get() = exception == null
}