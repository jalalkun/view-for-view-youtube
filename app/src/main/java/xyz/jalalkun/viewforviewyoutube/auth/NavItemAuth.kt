package xyz.jalalkun.viewforviewyoutube.auth

import xyz.jalalkun.viewforviewyoutube.utils.Cons.LOGIN
import xyz.jalalkun.viewforviewyoutube.utils.Cons.SPLASH

sealed class NavItemAuth(var route: String, var title: String){
    object Splash: NavItemAuth(SPLASH, "Splash")
    object Login: NavItemAuth(LOGIN, "Login")
}
