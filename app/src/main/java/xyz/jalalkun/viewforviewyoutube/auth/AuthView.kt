package xyz.jalalkun.viewforviewyoutube.auth

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.google.android.gms.common.SignInButton
import org.intellij.lang.annotations.JdkConstants
import viewforviewyoutube.R
import xyz.jalalkun.viewforviewyoutube.itemview.ErrorView
import xyz.jalalkun.viewforviewyoutube.ui.theme.ViewForViewComposeTheme
import xyz.jalalkun.viewforviewyoutube.utils.Loading

@Composable
fun AuthView(navHostController: NavHostController, viewModel: AuthViewModel) {
    var isLoginView by remember { mutableStateOf(true) }
    ViewForViewComposeTheme(darkTheme = false) {
        Scaffold {
            if (isLoginView) LoginView(viewModel = viewModel) {
                isLoginView = !isLoginView
            }
            else RegisterView(viewModel = viewModel) {
                isLoginView = !isLoginView
            }
        }
    }
}

@Composable
fun LoginView(viewModel: AuthViewModel, registerLoginChage: () -> Unit) {
    var email by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    var passwordVisibility by remember { mutableStateOf(false) }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Login",
            fontWeight = FontWeight.Bold,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            fontSize = 25.sp
        )

        Card(
            modifier = Modifier
                .padding(32.dp)
                .fillMaxWidth()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                if (viewModel.loading.value) {
                    Loading()
                }

                OutlinedTextField(
                    value = email,
                    onValueChange = {
                        email = it
                    },
                    label = { Text(text = stringResource(id = R.string.email_or_username)) },
                    modifier = Modifier.fillMaxWidth(),
                    trailingIcon = {
                        Icon(Icons.Default.Email, "")
                    },
                    maxLines = 1
                )
                OutlinedTextField(
                    value = password,
                    onValueChange = {
                        password = it
                    },
                    label = { Text(text = stringResource(id = R.string.password)) },
                    modifier = Modifier.fillMaxWidth()
                        .padding(bottom = 8.dp),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
                    trailingIcon = {
                        IconButton(onClick = {
                            passwordVisibility = !passwordVisibility
                        }) {
                            Icon(Icons.Default.Lock, "")
                        }
                    },
                    maxLines = 1
                )
                if (viewModel.wrongpassword.value){
                    ErrorView(stringResource(id = R.string.message_wrong_password))
                }
                OutlinedButton(
                    onClick = {
                        viewModel.login(username = email, password = password)
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp),
                    colors = ButtonDefaults.outlinedButtonColors(
                        backgroundColor = MaterialTheme.colors.secondary
                    ),
                    enabled = !viewModel.loading.value
                ) {
                    Text(
                        text = stringResource(id = R.string.login),
                        color = MaterialTheme.colors.onSurface
                    )
                }

                Text(
                    text = stringResource(id = R.string.or),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    textAlign = TextAlign.Center
                )

                if (viewModel.loading.value) {
                    GoogleLoginButton(
                        viewModel = viewModel,
                        mod = Modifier.fillMaxWidth(),
                        enable = false
                    )
                } else {
                    GoogleLoginButton(
                        viewModel = viewModel,
                        mod = Modifier.fillMaxWidth(),
                        enable = true
                    )
                }
            }
        }

        TextButton(
            onClick = { registerLoginChage() },
            enabled = !viewModel.loading.value
        ) {
            Text(
                text = stringResource(id = R.string.register),
                textAlign = TextAlign.Center,
                color = MaterialTheme.colors.primary,
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}


@Composable
fun RegisterView(viewModel: AuthViewModel, registerLoginChage: () -> Unit) {
    var email by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    var repeatPassword by rememberSaveable { mutableStateOf("") }
    var passwordVisibility by remember { mutableStateOf(false) }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.register),
            fontWeight = FontWeight.Bold,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            fontSize = 25.sp
        )

        Card(
            modifier = Modifier
                .padding(32.dp)
                .fillMaxWidth()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                if (viewModel.loading.value) {
                    Loading()
                }
                OutlinedTextField(
                    value = email,
                    onValueChange = {
                        email = it
                    },
                    label = { Text(text = stringResource(id = R.string.email)) },
                    modifier = Modifier.fillMaxWidth(),
                    trailingIcon = {
                        Icon(Icons.Default.Email, "")
                    },
                    maxLines = 1
                )
                OutlinedTextField(
                    value = password,
                    onValueChange = {
                        password = it
                    },
                    label = { Text(text = stringResource(id = R.string.password)) },
                    modifier = Modifier.fillMaxWidth(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
                    trailingIcon = {
                        IconButton(onClick = {
                            passwordVisibility = !passwordVisibility
                        }) {
                            Icon(Icons.Default.Lock, "")
                        }
                    },
                    maxLines = 1
                )
                OutlinedTextField(
                    value = repeatPassword,
                    onValueChange = {
                        repeatPassword = it
                    },
                    label = { Text(text = stringResource(id = R.string.repeat_password)) },
                    modifier = Modifier.fillMaxWidth(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                    visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
                    trailingIcon = {
                        IconButton(onClick = {
                            passwordVisibility = !passwordVisibility
                        }) {
                            Icon(Icons.Default.Lock, "")
                        }
                    },
                    maxLines = 1
                )
                OutlinedButton(
                    onClick = {
                        viewModel.loading()
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp),
                    colors = ButtonDefaults.outlinedButtonColors(
                        backgroundColor = MaterialTheme.colors.secondary
                    ),
                    enabled = !viewModel.loading.value
                ) {
                    Text(
                        text = stringResource(id = R.string.register),
                        color = MaterialTheme.colors.onSurface
                    )
                }
            }
        }

        TextButton(
            onClick = { registerLoginChage() },
            enabled = !viewModel.loading.value
        ) {
            Text(
                text = stringResource(id = R.string.login),
                textAlign = TextAlign.Center,
                color = MaterialTheme.colors.primary,
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold
            )
        }
    }
}

@Composable
fun GoogleLoginButton(viewModel: AuthViewModel, mod: Modifier, enable: Boolean) {
    Log.e("LOGIN", "GoogleLoginButton: enable: $enable")
    val button = SignInButton(LocalContext.current)
    button.isEnabled = enable
    button.setOnClickListener {
        /*TODO login with google account*/
        Log.e("LOGIN", "GoogleLoginButton: onclick")
        viewModel.loading()
    }
    AndroidView(
        factory = { button },
        modifier = mod
    )
}

@Composable
@Preview
fun LoginPrev() {
    AuthView(
        navHostController = NavHostController(LocalContext.current),
        viewModel = AuthViewModel()
    )
}