package xyz.jalalkun.viewforviewyoutube.auth

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import xyz.jalalkun.viewforviewyoutube.main.MainActivity
import xyz.jalalkun.viewforviewyoutube.ui.theme.ViewForViewComposeTheme

class AuthActivity : ComponentActivity() {
    val viewModel by viewModels<AuthViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ViewForViewComposeTheme {
                MainScreen(viewModel)
            }
        }
        observe()
    }

    private fun observe(){
        if (viewModel.moveToMain.value) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }
}

@Composable
fun MainScreen(viewModel: AuthViewModel) {
    val navController = rememberNavController()
    viewModel.init(navController)
    NavigationAuth(navHostController = navController, viewModel = viewModel)
}

@Composable
fun NavigationAuth(navHostController: NavHostController, viewModel: AuthViewModel){
    NavHost(navController = navHostController, startDestination = NavItemAuth.Splash.route){
        composable(NavItemAuth.Splash.route) {
            Splash(navHostController = navHostController, viewModel = viewModel)
        }
        composable(NavItemAuth.Login.route){
            AuthView(navHostController = navHostController, viewModel = viewModel)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview3() {
    ViewForViewComposeTheme {
        MainScreen(AuthViewModel())
    }
}