package xyz.jalalkun.viewforviewyoutube.auth

import android.content.Intent
import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavHostController
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import xyz.jalalkun.viewforviewyoutube.base.BaseViewModel
import xyz.jalalkun.viewforviewyoutube.main.MainActivity
import xyz.jalalkun.viewforviewyoutube.utils.Cons.LOGIN
import xyz.jalalkun.viewforviewyoutube.utils.DummyData
import xyz.jalalkun.viewforviewyoutube.utils.Sessions

class AuthViewModel: BaseViewModel() {
    private var loginStatus = false
    private var navHostController: NavHostController? = null
    var wrongpassword = mutableStateOf(false)
        private set
    var moveToMain = mutableStateOf(false)
        private set
    var loading = mutableStateOf(false)
        private set

    fun loading() {
        scope.launch(main) {
            loading.value = true
            delay(5000)
            loading.value = false
        }
    }

    fun login(username: String, password: String){
        if (username == "saucang" || username == "saucang@gmail.com" && password == "saucangenak"){
            wrongpassword.value = false
            DummyData.fakeUser()
            checkIsLogin()
        }else{
            wrongpassword.value = true
        }
    }

    fun init(nvc: NavHostController) {
        navHostController = nvc
    }

    fun checkIsLogin() {
        Log.e("AuthViewModel", "checkIsLogin: ${Sessions.getUser()}", )
        if (Sessions.getUser() != null){
            loginStatus = true
        }
        moveView()
    }

    private fun moveView() {
        Log.e("AuthViewModel", "moveView: loginStatus $loginStatus" )
        if (loginStatus){
            Log.e("AuthViewModel", "moveView: to main $loginStatus" )
            navHostController?.context?.startActivity(Intent(navHostController?.context, MainActivity::class.java))
        }else{
            Log.e("AuthViewModel", "moveView: to login")
            navHostController?.navigate(LOGIN) {
                popUpTo(NavItemAuth.Login.route) { inclusive = true }
                popUpTo(NavItemAuth.Splash.route) { inclusive = true }
            }
        }
    }
}