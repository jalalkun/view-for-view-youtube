package xyz.jalalkun.viewforviewyoutube.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import xyz.jalalkun.viewforviewyoutube.utils.Utils.isDark

private val DarkColorPalette = darkColors(
    primary = DarkPrimary,
    primaryVariant = DarkPrimaryDark,
    secondary = DarkSecondary,
    secondaryVariant = DarkSecondaryDark,
    background = DarkPrimaryDark,
    surface = DarkPrimaryLight,
    onBackground = DarkPrimaryText,
    onSurface = DarkPrimaryText,
    onPrimary = DarkPrimaryText,
    onSecondary = DarkPrimaryText,
    onError = ErrorColor
)

private val LightColorPalette = lightColors(
    primary = PrimaryBlue,
    primaryVariant = PrimaryBlueDark,
    secondary = SecondaryAmber,
    secondaryVariant = SecondaryAmberDark,
    background = SecondaryAmber,
    surface = PrimaryBlueLight,
    onSurface = PrimaryTextColor,
    onBackground = PrimaryTextColor,
    onSecondary = SecondaryTextColor,
    onPrimary = PrimaryTextColorDark,
    onError = ErrorColor

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Composable
fun ViewForViewComposeTheme(
    darkTheme: Boolean = isDark(),
    content: @Composable() () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}