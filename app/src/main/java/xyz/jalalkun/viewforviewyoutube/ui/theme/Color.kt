package xyz.jalalkun.viewforviewyoutube.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val ErrorColor = Color(0xFFe53935)
/**
 * Warna dari material color system
 *
 * [https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=03A9F4&secondary.color=FFC400]
 */
val PrimaryBlue = Color(0xFF03A9F4)
val PrimaryBlueLight = Color(0xFF67DAFF)
val PrimaryBlueDark = Color(0xFF007AC1)
val SecondaryAmber = Color(0xFFFFC400)
val SecondaryAmberLight = Color(0xFFFFF64F)
val SecondaryAmberDark = Color(0xFFC79400)
val PrimaryTextColor = Color(0xFF000000)
val PrimaryTextColorDark = Color(0xFFFFFFFF)
val SecondaryTextColor = Color(0xFF000000)
val SecondaryTextColorDark = Color(0xFFFFFFFF)

/**
 * Dark theme dari
 *
 * [https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=424242&secondary.color=37474F]
 */

val DarkPrimary = Color(0xFF424242)
val DarkPrimaryLight = Color(0xFF6d6d6d)
var DarkPrimaryDark = Color(0xFF1b1b1b)
val DarkSecondary = Color(0xFF37474f)
val DarkSecondaryLight = Color(0xFF62727b)
val DarkSecondaryDark = Color(0xFF102027)
val DarkPrimaryText = Color(0xFFffffff)
val DarkSecondaryText = Color(0xFFffffff)