package xyz.jalalkun.viewforviewyoutube.play

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import xyz.jalalkun.viewforviewyoutube.ui.theme.ViewForViewComposeTheme

class PlayActivity : ComponentActivity() {
    private val viewmodel by viewModels<PlayViewModel>()
    private val tag = "PlayActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ViewForViewComposeTheme(false) {
                Scaffold {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight()
                            .padding(16.dp)
                    ) {
                        Konten(viewmodel)
                    }
                }
            }
        }
    }
    private fun loadData(){
        object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.e("PlayActivity on tick","seconds remaining: " + millisUntilFinished / 1000)
            }

            override fun onFinish() {
                viewmodel.isLoading = false
            }
        }.start()

//        api.getMovies().enqueue(object : Callback<List<Movie>>{
//            override fun onResponse(call: Call<List<Movie>>, response: Response<List<Movie>>) {
//                if (response.body()!=null){
//                    datas = response.body()!!
//                    isLoading.value = false
//                }
//            }
//
//            override fun onFailure(call: Call<List<Movie>>, t: Throwable) {
//                Toast.makeText(this@PlayActivity, t.localizedMessage, Toast.LENGTH_SHORT).show()
//            }
//
//        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Log.e(tag, "onBackPressed: " )
        viewmodel.onDestroy = true
    }

    override fun onDestroy() {
        viewmodel.onDestroy = true
        super.onDestroy()
        Log.e(tag, "onDestroy: " )
    }
}

@Composable
fun Konten(viewModel: PlayViewModel) {
    PlayScreen(viewModel)
}




@Preview(showBackground = true)
@Composable
fun DefaultPreview2() {
    ViewForViewComposeTheme {
    }
}