package xyz.jalalkun.viewforviewyoutube.play

import android.os.CountDownTimer
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import xyz.jalalkun.viewforviewyoutube.base.BaseViewModel
import xyz.jalalkun.viewforviewyoutube.extensions.*
import xyz.jalalkun.viewforviewyoutube.model.HistoryView
import xyz.jalalkun.viewforviewyoutube.utils.Utils

class PlayViewModel: BaseViewModel() {
    var isLoading by mutableStateOf(false)
    var videoId by mutableStateOf("")
    var historyViews = mutableStateListOf<HistoryView>()
    var totalPoint by mutableStateOf(0)
    var onDestroy by mutableStateOf(false)

    fun addHistoriView(historyView: HistoryView){
        totalPoint += historyView.point!!
        historyViews.add(historyView)
    }

    fun getMovie(){
        ioJob {
            doNetworkCall { movieApi.getMovies() }
                .awaitForResult()
                .onSuccessResult {  }
                .onFailureWithErrorResult {  }
        }
    }

    fun getNewId(){
        isLoading = true
        object : CountDownTimer(4000, 1000){
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                videoId = Utils.getRandomId()
                isLoading = false
            }

        }.start()
    }
}