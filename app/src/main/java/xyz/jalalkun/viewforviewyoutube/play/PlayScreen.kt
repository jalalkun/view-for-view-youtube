package xyz.jalalkun.viewforviewyoutube.play

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import xyz.jalalkun.viewforviewyoutube.model.HistoryView
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun PlayScreen(
    viewModel: PlayViewModel
) {
    Log.e("PlayScreen", "PlayScreen: loading: ${viewModel.isLoading}")
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        if (viewModel.isLoading) {
            Spacer(Modifier.height(30.dp))
            LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
        } else Youtube(viewModel)
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            Text(
                text = viewModel.totalPoint.toString(),
                fontSize = 32.sp,
                fontWeight = FontWeight.Bold
            )
        }
        LazyColumn(modifier = Modifier.fillMaxWidth()) {
            var first = 0
            var last = viewModel.historyViews.lastIndex
            if (viewModel.historyViews.lastIndex > 15) {
                last = viewModel.historyViews.lastIndex
                first = last - 15
            }
            items(items = viewModel.historyViews, itemContent = { posisi ->
                Card(
                    modifier = Modifier
                        .padding(bottom = 8.dp)
                        .clip(shape = RoundedCornerShape(4.dp))
                        .fillMaxWidth()
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                start = 8.dp,
                                end = 8.dp,
                                top = 4.dp,
                                bottom = 4.dp
                            )
                    ) {
                        Text(text = posisi.time_create.toString())
                        Text(
                            text = posisi.point.toString(),
                            modifier = Modifier.fillMaxWidth(),
                            textAlign = TextAlign.End
                        )
                    }
                }
            })
        }
    }
}

@Composable
fun Youtube(viewModel: PlayViewModel) {
    Log.e("PlayScreen", "Youtube: ")
    val tag = "Youtube"
    val context = LocalContext.current
    val yt = YouTubePlayerView(context)
    val playControl = yt.getPlayerUiController()
    playControl.showPlayPauseButton(false)
    playControl.showSeekBar(false)
    playControl.showYouTubeButton(false)
    yt.addYouTubePlayerListener(object : YouTubePlayerListener {
        override fun onApiChange(youTubePlayer: YouTubePlayer) {
        }

        override fun onCurrentSecond(youTubePlayer: YouTubePlayer, second: Float) {
        }

        override fun onError(youTubePlayer: YouTubePlayer, error: PlayerConstants.PlayerError) {
            Log.e(tag, "onError: ${error.name}")
        }

        override fun onPlaybackQualityChange(
            youTubePlayer: YouTubePlayer,
            playbackQuality: PlayerConstants.PlaybackQuality
        ) {
            when (playbackQuality) {
                PlayerConstants.PlaybackQuality.DEFAULT -> Log.e(
                    tag,
                    "onPlaybackQualityChange: DEFAULT"
                )
                PlayerConstants.PlaybackQuality.SMALL -> Log.e(
                    tag,
                    "onPlaybackQualityChange: SMALL"
                )
                PlayerConstants.PlaybackQuality.MEDIUM -> Log.e(
                    tag,
                    "onPlaybackQualityChange: MEDIUM"
                )
                PlayerConstants.PlaybackQuality.LARGE -> Log.e(
                    tag,
                    "onPlaybackQualityChange: LARGE"
                )
                PlayerConstants.PlaybackQuality.HIGH_RES -> Log.e(
                    tag,
                    "onPlaybackQualityChange: HIGH_RES"
                )
                PlayerConstants.PlaybackQuality.HD720 -> Log.e(
                    tag,
                    "onPlaybackQualityChange: HD721"
                )
                PlayerConstants.PlaybackQuality.HD1080 -> Log.e(
                    tag,
                    "onPlaybackQualityChange: HD1080"
                )
                PlayerConstants.PlaybackQuality.UNKNOWN -> Log.e(
                    tag,
                    "onPlaybackQualityChange: UNKNOWN"
                )
                else -> Log.e(tag, "onPlaybackQualityChange: UNKNOWN")
            }
        }

        override fun onPlaybackRateChange(
            youTubePlayer: YouTubePlayer,
            playbackRate: PlayerConstants.PlaybackRate
        ) {
            Log.e(tag, "onPlaybackRateChange: ")
        }

        override fun onReady(youTubePlayer: YouTubePlayer) {
            Log.e(tag, "onReady: ")
            if (viewModel.videoId.isNotEmpty())
                youTubePlayer.loadVideo(viewModel.videoId, 0f)
            else
                viewModel.getNewId()
        }

        override fun onStateChange(
            youTubePlayer: YouTubePlayer,
            state: PlayerConstants.PlayerState
        ) {
            when (state) {
                PlayerConstants.PlayerState.UNSTARTED -> {
                    Log.e(tag, "onStateChange: UNSTARTED")
                }
                PlayerConstants.PlayerState.BUFFERING -> {
                    Log.e(tag, "onStateChange: BUFFERING")
                }
                PlayerConstants.PlayerState.PAUSED -> {
                    Log.e(tag, "onStateChange: PAUSED")
                }
                PlayerConstants.PlayerState.PLAYING -> {
                    Log.e(tag, "onStateChange: PLAYING")
                }
                PlayerConstants.PlayerState.ENDED -> {
                    Log.e(tag, "onStateChange: ENDED")
                    val simpleDateFormat = SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z")
                    val currentDateAndTime: String = simpleDateFormat.format(Date())
                    viewModel.addHistoriView(
                        HistoryView(
                            id = viewModel.historyViews.size.toString(),
                            point = 2,
                            campaign_id = UUID.randomUUID().toString(),
                            time_create = currentDateAndTime
                        )
                    )
                    viewModel.videoId = ""
                    viewModel.getNewId()
                }
                PlayerConstants.PlayerState.UNKNOWN -> {
                    Log.e(tag, "onStateChange: UNKNOWN")
                }
                PlayerConstants.PlayerState.VIDEO_CUED -> {
                    Log.e(tag, "onStateChange: VIDEO_CUED")
                }
            }
        }

        override fun onVideoDuration(youTubePlayer: YouTubePlayer, duration: Float) {
            Log.e(tag, "onVideoDuration: $duration")
        }

        override fun onVideoId(youTubePlayer: YouTubePlayer, videoId: String) {
            Log.e(tag, "onVideoId: $videoId")
        }

        override fun onVideoLoadedFraction(youTubePlayer: YouTubePlayer, loadedFraction: Float) {
        }
    })
    AndroidView(
        modifier = Modifier
            .fillMaxWidth()
            .height(250.dp),
        factory = {
            yt
        }
    )
    if (viewModel.onDestroy) {
        Log.e("PlayScreen", "Youtube: onDestroy", )
        yt.release()
    }
}