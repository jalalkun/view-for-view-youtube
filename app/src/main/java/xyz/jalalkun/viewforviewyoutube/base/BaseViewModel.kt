package xyz.jalalkun.viewforviewyoutube.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.coroutineScope
import xyz.jalalkun.viewforviewyoutube.network.MovieInterface
import xyz.jalalkun.viewforviewyoutube.network.movieApiService
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel() {
    val scope = MainScope()
    open val main: CoroutineContext by lazy { Dispatchers.Main }
    open val io: CoroutineContext by lazy { Dispatchers.IO }
    val movieApi : MovieInterface = movieApiService
}