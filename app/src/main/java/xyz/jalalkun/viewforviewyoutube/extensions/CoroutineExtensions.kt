package xyz.jalalkun.viewforviewyoutube.extensions

import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import kotlinx.coroutines.*
import retrofit2.Response
import xyz.jalalkun.viewforviewyoutube.base.BaseViewModel
import xyz.jalalkun.viewforviewyoutube.model.ApiResult
import xyz.jalalkun.viewforviewyoutube.model.ErrorResult
import xyz.jalalkun.viewforviewyoutube.model.Rest
import xyz.jalalkun.viewforviewyoutube.utils.Utils.notNull

suspend fun <R> BaseViewModel.doNetworkCall(block: suspend () -> Response<R>): Deferred<Response<R>> {
    return viewModelScope.async(io) { block.invoke() }
}

suspend fun <R> Deferred<Response<R>>.awaitForResult(): ApiResult<R> {
    val result = ApiResult<R>()
    try {
        val response = await()
        result.response = response
        result.data = response.body()
    } catch (ex: Exception) {
        result.exception = ex
    } finally {
        return result
    }
}

fun <T> Response<T>.errorResult(): ErrorResult? {
    val stringError = errorBody()?.string()
    if (stringError.isNullOrEmpty()) return null
    return try {
        val result = Gson().fromJson(stringError, Rest::class.java)
        result.message?.let { ErrorResult(it, code()) }
    } catch (exception: Exception) {
        ErrorResult(stringError, code())
    }
}

fun <R> ApiResult<R>.onSuccessResult(block: (R) -> Unit): ApiResult<R> {
    if (this.isSuccess) {
        response?.body()?.notNull { block(it) }
    }
    return this
}

fun <R> ApiResult<R>.onSuccessResponse(block: (Response<R>) -> Unit): ApiResult<R> {
    if (this.isSuccess && this.response != null) {
        response?.notNull { block(it) }
    }
    return this
}

fun <R> ApiResult<R>.onFailure(block: (Exception) -> Unit) {
    if (!this.isSuccess) {
        exception?.notNull { block(it) }
    }
}

fun <R> ApiResult<R>.onFailureWithErrorResult(block: (ErrorResult) -> Unit) {
    if (!this.isSuccess) {
        exception.notNull {
            val errorMessage = it.localizedMessage ?: "Oops, something when wrong"
            val errorCode = response?.code() ?: 0
            block(ErrorResult(errorMessage, errorCode))
        }
    } else {
        response?.errorResult().notNull { block(it) }
    }
}


fun BaseViewModel.uiJob(block: suspend CoroutineScope.() -> Unit): Job {
    return viewModelScope.launch(main) {
        block()
    }
}

fun BaseViewModel.ioJob(block: suspend CoroutineScope.() -> Unit): Job {
    return viewModelScope.launch(io) {
        block()
    }
}

fun launchMain(block: suspend () -> Unit) {
    GlobalScope.launch(Dispatchers.Main) {
        block.invoke()
    }
}