package xyz.jalalkun.viewforviewyoutube

import android.app.Application
import xyz.jalalkun.viewforviewyoutube.utils.Sessions

class MyApp: Application() {
    override fun onCreate() {
        super.onCreate()
        Sessions.init(this)
    }
}